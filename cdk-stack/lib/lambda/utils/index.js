
const getResponse = (code = 200, data = {}) => {
  return {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    statusCode: code,
    body: JSON.stringify(data)
  };
};

const getRedirectResponse = (url) => {
  return {
    headers: {
      'location': url
    },
    statusCode: 307,
    body: ''
  };
};

module.exports = {
  getResponse,
  getRedirectResponse
};
