
const isProd = () => process.env.STAGE === 'prod';
const getRemovalPolicy = () => isProd() ? 'RETAIN' : 'DESTROY';

module.exports = {
  isProd,
  getRemovalPolicy
};
