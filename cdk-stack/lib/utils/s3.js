
const iam = require('@aws-cdk/aws-iam');
const AWS_ACCOUNT = process.env.AWS_ACCOUNT;

const addIpLimitPolicy = (bucket) => {

  bucket.addToResourcePolicy(
    new iam.PolicyStatement({
      actions: ['s3:GetObject'],
      conditions: {
        IpAddress: {
          'aws:SourceIp': [
            '172.96.97.0/24',
            '54.240.217.16/29',
            '72.21.196.64/29',
            '54.240.217.8/29',
            '72.21.198.64/29',
            '54.240.217.0/27'
          ]
        }
      },
      principals: [new iam.AnyPrincipal()],
      resources: [bucket.arnForObjects('*')]
    })
  );

  bucket.addToResourcePolicy(
    new iam.PolicyStatement({
      actions: ['s3:*'],
      principals: [new iam.AccountPrincipal(AWS_ACCOUNT)],
      resources: [bucket.bucketArn]
    })
  );

  bucket.addToResourcePolicy(
    new iam.PolicyStatement({
      actions: ['s3:*'],
      principals: [new iam.AccountPrincipal(AWS_ACCOUNT)],
      resources: [bucket.arnForObjects('*')]
    })
  );

};

const getCors = () => {

  return [
    {
      allowedHeaders: [
        '*'
      ],
      allowedMethods: [
          'POST',
          'GET',
          'PUT',
          'DELETE',
          'HEAD'
      ],
      allowedOrigins: [
          '*',
          'http://*',
          'https://*'
      ],
      maxAge: 3000
    }
  ];

};

module.exports = { addIpLimitPolicy, getCors };
