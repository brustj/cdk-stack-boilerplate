
const enableAutoScaling = (table) => {

  const autoScaleMinMax = {
    minCapacity: 5,
    maxCapacity: 40000
  };
  const utilizationPercentage = {
    targetUtilizationPercent: 70
  };

  const autoScaleRead = table.autoScaleReadCapacity(autoScaleMinMax);
  autoScaleRead.scaleOnUtilization(utilizationPercentage);

  const autoScaleWrite = table.autoScaleWriteCapacity(autoScaleMinMax);
  autoScaleWrite.scaleOnUtilization(utilizationPercentage);

};

module.exports = { enableAutoScaling };
