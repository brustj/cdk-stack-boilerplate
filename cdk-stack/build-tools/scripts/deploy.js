
/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */

require('shelljs/global');

const chalk = require('chalk');
const inquirer = require('inquirer');
const { execSync } = require('child_process');
const { 
  awsAccounts, 
  awsProfiles
} = require('../../package.json');

const getUserInputs = () =>
  new Promise((resolve) => {
    inquirer
      .prompt([
        {
          type: 'list',
          name: 'stage',
          message: 'Which stage would you like to publish to?',
          choices: ['dev', 'prod']
        },
        {
          type: 'input',
          name: 'confirmProd',
          message: `Type the word ${chalk.whiteBright.underline('prod')} to confirm that you want to deploy to the ${chalk.whiteBright('production')} AWS account...`,
          default: '',
          when: (prevInput) => prevInput.stage === 'prod'
        }
      ])
      .then((answers) => {

        const {
          stage,
          confirmProd
        } = answers;

        if (
          (stage === 'dev') ||
          ((stage === 'prod') && (confirmProd === 'prod'))
        ) {
          resolve(answers);
        } else {
          abortDeploy();
        }

      });
  });

const deployBackend = ({stage}) =>
  new Promise((resolve) => {
    try {

      console.log(`\nDeploying to ${chalk.underline(stage)}...\n`);

      execSync(
        `AWS_ACCOUNT=${awsAccounts[stage]} STAGE=${stage} cdk deploy --profile ${awsProfiles[stage]}`,
        {
          stdio: 'inherit'
        }
      );

      resolve();

    } catch (err) {
      reject(err);
    }
  });

getUserInputs()
  .then((inputs) => deployBackend(inputs))
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(`\n${chalk.redBright.bold('Deploy aborted.')}\n`);
    throw err;
  });
