
/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */

require('shelljs/global');

const chalk = require('chalk');
const inquirer = require('inquirer');
const {
  getLambdaFns,
  buildFns
} = require('../utils');

const getUserInputs = (fns) =>
  new Promise((resolve) => {
    inquirer
      .prompt([
        {
          type: 'checkbox',
          name: 'fns',
          message: 'Which function(s) would you like to build?',
          choices: fns
        }
      ])
      .then((answers) => resolve(answers));
  });

const init = async () => {
  try {
    const fns = await getLambdaFns('./lib/lambda/functions'),
          inputs = await getUserInputs(fns);

    await buildFns(inputs.fns);

    console.log(`${chalk.cyanBright.underline(`Lambda build is complete!`)}\n`);
    process.exit(0);
  } catch (err) {
    console.log(`\n${chalk.redBright.bold('Lambda build failed.')}\n`);
    console.log(err);
    process.exit(0);
  }
};

init();
