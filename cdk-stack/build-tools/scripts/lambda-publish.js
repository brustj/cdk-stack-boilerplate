
/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */

require('shelljs/global');

const chalk = require('chalk');
const inquirer = require('inquirer');
const {
  getLambdaFns,
  buildFns,
  publishFns
} = require('../utils');

const getUserInputs = (fns) =>
  new Promise((resolve) => {
    inquirer
      .prompt([
        {
          type: 'checkbox',
          name: 'fns',
          message: 'Which function(s) would you like to publish?',
          choices: fns
        },
        {
          type: 'list',
          name: 'stage',
          message: 'Which stage would you like to publish to?',
          choices: ['dev', 'prod']
        }
      ])
      .then((answers) => resolve(answers));
  });

const init = async () => {
  try {
    const fns = await getLambdaFns('./lib/lambda/functions'),
          inputs = await getUserInputs(fns);

    await buildFns(inputs.fns);
    await publishFns(inputs);

    console.log(`${chalk.cyanBright.underline(`Lambda publish is complete!`)}\n`);
    process.exit(0);
  } catch (err) {
    console.log(`\n${chalk.redBright.bold('Lambda publish failed.')}\n`);
    console.log(err);
    process.exit(0);
  }
};

init();
