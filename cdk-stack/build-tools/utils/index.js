
const { execSync } = require('child_process');
const { readdirSync } = require('fs');
const { awsProfiles } = require('../../package.json');

// read lambda functions from local folder
const getLambdaFns = (path) =>
  new Promise((resolve, reject) => {
    try {
      resolve(
        readdirSync(path)
          .sort()
          .filter((fn) => fn !== '.DS_Store')
      );
    } catch (err) {
      reject(err);
    }
  });

// compile and build function to dist folder
const buildFns = (fns = []) =>
  new Promise((resolve, reject) => {
    execSync(`rm -rf lib/lambda/dist && mkdir lib/lambda/dist`);
    const fnPromises = fns.map((fn) =>
      new Promise((resolve, reject) => {
        try {
          execSync(`cp -r lib/lambda/functions/${fn}/ lib/lambda/dist/${fn}/`);
          execSync(`esbuild lib/lambda/functions/${fn}/index.js --bundle --platform=node --target=node14 --external:aws-sdk --external:dynamoose --external:shortid --external:slugify --external:sharp --outfile=lib/lambda/dist/${fn}/index.js`);
          resolve();
        } catch (err) {
          reject(err);
        }
      })
    );

    Promise.all(fnPromises)
      .then((data) => resolve(data))
      .catch((err) => reject(err));

  });


// publish compiled functions to aws account
const publishFns = ({stage, fns = []}) =>
  new Promise((resolve, reject) => {
    const fnPromises = fns.map((fn) =>
      new Promise((resolve, reject) => {
        try {

          zipFn(fn)
            .then(() => uploadFn(stage, fn))
            .then(() => resolve())
            .catch((err) => reject(err));

        } catch (err) {
          reject(err);
        }

      })
    );

    Promise.all(fnPromises)
      .then((data) => resolve(data))
      .catch((err) => reject(err));
  });

// zip function and place in temp local folder
const zipFn = (fn) =>
  new Promise((resolve, reject) => {
    try {
      execSync(`
        cd lib/lambda/dist/${fn} &&
        rm -rf .tmp &&
        mkdir .tmp &&
        zip -r .tmp/index.zip ./ &&
        cd ../../../..
      `);

      resolve();
    } catch (err) {
      reject(err);
    }
  });

// upload zipped function to lambda and publish new version
const uploadFn = (stage = 'dev', fn) =>
  new Promise((resolve, reject) => {
    try {
      execSync(`aws lambda update-function-code --function-name ${fn} --zip-file fileb://lib/lambda/dist/${fn}/.tmp/index.zip --region us-east-1 --profile ${awsProfiles[stage]}`);
      removeTempFolder(fn);

      resolve();
    } catch (err) {
      reject(err);
    }
  });

// delete temp local folder
const removeTempFolder = (fn) => {
  execSync(`rm -r lib/lambda/dist/${fn}/.tmp`);
};

module.exports = {
  getLambdaFns,
  buildFns,
  publishFns
};
